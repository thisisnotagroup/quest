terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.20.0"
    }
  }
}

provider "cloudflare" {
  #  email     = var.cloudflare_account_email
  api_token = var.api_token
}

provider "aws" {
  #  profile = "lab_power"
  region = "us-east-2"
}


