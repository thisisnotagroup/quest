# eip

resource "aws_eip" "eip" {
  vpc = true
}



# internet gateway

resource "aws_internet_gateway" "dev_gw" {
  vpc_id = aws_vpc.vpc_dev.id
}



# route table

resource "aws_route_table" "route_table_dev" {
  vpc_id = aws_vpc.vpc_dev.id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dev_gw.id
  }
}



# route table association

resource "aws_route_table_association" "route_association" {
  subnet_id      = aws_subnet.public_subnet_001.id
  route_table_id = aws_route_table.route_table_dev.id
}



# vpc

resource "aws_vpc" "vpc_dev" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
}



# create public subnet

resource "aws_subnet" "public_subnet_001" {
  cidr_block        = cidrsubnet(aws_vpc.vpc_dev.cidr_block, 3, 1)
  vpc_id            = aws_vpc.vpc_dev.id
  availability_zone = var.aws_az
}

resource "aws_subnet" "public_subnet_000" {
  cidr_block        = cidrsubnet(aws_vpc.vpc_dev.cidr_block, 6, 1)
  vpc_id            = aws_vpc.vpc_dev.id
  availability_zone = var.aws_az_1
}


# create security group

resource "aws_security_group" "ingress_dev" {
  name   = "allow_all_sg"
  vpc_id = aws_vpc.vpc_dev.id

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]

    from_port = 0
    to_port   = 0
    protocol  = "-1"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


}
