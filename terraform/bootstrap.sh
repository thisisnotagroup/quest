#!/bin/bash -x

yum check-update
yum update -y

# install docker prereqs
yum install -y \
   git \
   vim

# remove docker

yum remove docker -y \
   docker-client \
   docker-client-latest \
   docker-common \
   docker-latest \
   docker-latest-logrotate \
   docker-logrotate \
   docker-engine 

# install repo

yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sed -i 's/$releasever/7/g' /etc/yum.repos.d/docker-ce.repo
yum update -y

# install dependencies

yum install -y yum-plugin-copr
yum install -y copr enable lsm5/container-selinux
yum install -y fuse-overlayfs slirp4netns
yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.21-1.el7.noarch.rpm
rpm -Va --nofiles --nodigest

# install docker

yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

systemctl start docker

git clone https://gitlab.com/thisisnotagroup/quest.git

cd quest

# docker build
docker build -t rearc/quest:0.1 .

# docker run
docker run -dp 3000:3000 rearc/quest:0.1
