# create ec2 t2.smol instance



# aws instance

resource "aws_instance" "ec2_dev" {
  ami                    = var.aws_ami
  instance_type          = var.aws_instance_size
  vpc_security_group_ids = [aws_security_group.ingress_dev.id]
  subnet_id              = aws_subnet.public_subnet_001.id
  user_data              = data.template_file.user_data.rendered
  key_name               = aws_key_pair.deployer.key_name
}

resource "aws_key_pair" "deployer" {
  key_name = "key-dev"

  # aws_key.pub
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDwOGjOo8wfHjr7bBhj6P87hJz3Vk0tgBZhEmrLyZ7fCQXtK0joMipzL02X3eZpH8lLPBoCnBy4/8YoFnoctv2sx6s+/LmbA3qCRBtYdiJO9GvaWisH9RhF8PL/43y8+qZ5N+VABStNSL5TY7EtkB4IdHo82c6lR8oPA7naA74DUkBU9gOsKnIhnxq08aKag+2gZJDVrvmcwqkJ/kcjuIoLB1qniPUqTDez80ceEhCJhwMlJoLl1qlBnwoX5atZ4uW4edYQuZcn1S8ySi3kHHGSv/8u101HnHRe6bfVul6PJV/W1OJTs0CyMIc9mqreFW8/AoTQ+oOWpV4p3Dof6sVGlBYNUSRapQG2JWUgIs9rvE26ZiGcyiv2MIKCvLXRw/Mzc8IY8RPlwvazWRV10FBW3uzYG6zEa+iCfwJHCRRjzJIS2z3QjFapMpkdYR/6aJiPgNUEFJQB3JfZIMpEa9mkTCTFzyUkWdNxmnVjHYKkcwxswQZjauszrqDxLXwtG8="
}

# eip association with instance

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.ec2_dev.id
  allocation_id = aws_eip.eip.id
}

