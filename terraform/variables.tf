variable "domain_name" {
  description = "domain name"
  type        = string
}

variable "api_token" {
}

variable "api_key" {
}

variable "cloudflare_account_email" {
  description = "cloudflare account email"
  type        = string
}

variable "aws_region" {
  description = "aws region"
  type        = string
  default     = "us-west-1"
}

variable "aws_az" {
  description = "aws availability zone"
  type        = string
  default     = "us-west-1b"
}

variable "aws_az_1" {
  type    = string
  default = "us-west-1c"
}

variable "aws_ami" {
  description = "aws machine image"
  type        = string
  default     = "ami-09bedd705318020ae"
}

variable "aws_instance_size" {
  description = "aws instance type"
  type        = string
  default     = "t2.small"
}
