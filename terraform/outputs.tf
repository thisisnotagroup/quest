output "ec2_eip" {
  value = aws_eip.eip.public_ip
}

output "ec2_dns" {
  value = aws_eip.eip.public_dns
}

#output "acm_dns_name" {
#  value = aws_acm_certificate.cert.resource_record_name
#}

#output "acm_dns_type" {
#  value = aws_acm_certificate.cert.resource_record_type
#}

#output "acm_dns_value" {
#  value = aws_acm_certificate.cert.resource_record_value
#}
