# A quest in the clouds, by Cameron McDougle

### 3. An answer to the prompt: "Given more time, I would improve..."

#### I.
As I'm writing this, I'm reading through implementing Terraform for a CloudFlare Load Balancer to go above and beyond with something way too fancy, a multi-cloud implementation on AWS and GCP, load balanced with CloudFlare. 

`terraform/zones.tf`
```
resource "cloudflare_zone" "zone" {
  zone = var.domain_name
}
```

I'm realizing there's one significant flaw: that CloudFlare, while able to perform health checks, won't be able to actually *do* anything about it should the health check fail. 

There's absolutely a way to overcome this, but I'm realizing I (probably) don't have time to implement it right now, given I haven't even started on the actual quest.

So far, I've spent all my time implementing CI/CD for Terraform in GitLab and setting DNS records in CloudFlare for the demo site, sexbobomb.net (bonus points if you know the reference! :D).

`gitlab-ci.yml`
```
stages:
- validate
- test
- build
- deploy
sast:
  stage: test
fmt:
  extends: .terraform:fmt
  needs: []
validate:
  extends: .terraform:validate
  needs: []

before_script:
#  - export TF_VAR_api_token=${api_token}
  - export TF_VAR_cloudflare_account_email=${cloudflare_account_email}
  - export TF_VAR_domain_name=${domain_name}
  - export TF_VAR_api_key=${api_key}
build:
  extends: .terraform:build
deploy:
  extends: .terraform:deploy
  dependencies:
    - build
  environment:
    name: $TF_STATE_NAME
include:
- template: Security/SAST.gitlab-ci.yml
- template: Terraform/Base.gitlab-ci.yml
- template: Jobs/SAST-IaC.gitlab-ci.yml
variables:
  TF_STATE_NAME: default
  TF_CACHE_KEY: default
  TF_ROOT: terraform/
  api_token: $api_token
  cloudflare_account_email: $cloudflare_account_email
  domain_name: $domain_name
```
Pipeline screenshot:

![gitlab pipeline](./img/gitlab-pipeline.png)

So, given more time, I would totally implement it (and likely will soon anyway). 

#### II.
In the same vein, I'm likely not going to have time to put everything into nice modules. Modules are nice.

#### III.
`RUN npm ci --only=production` in `Dockerfile` seems cool and I'd like to know more about it. It builds a more reliable, faster, reproducible build for prod.

Dockerfile:
```
FROM node:16

WORKDIR /app

COPY package.json ./
RUN npm install

COPY . .

EXPOSE 3000
CMD [ "node", "src/000.js" ]
```

#### IV.
It seemed a lot easier to mirror the repo to GCP and have the image building occur there than to do it in GitLab and store and push the image (docker-in-docker, just a lot more moving parts and redundant image storage etc).

#### V.
Problems. At this point in time, my GitLab repo pushes any changes (mirrors) to Google Cloud Source Repositories. 
Any pushes trigger a Cloud Build trigger which builds the Dockerfile into an image and *should* push that image into either the Artifacts Registry or Container Registry (I've tried both).

`cloudbuild.yaml`
```
steps:
- name: 'gcr.io/cloud-builders/docker'
  args: [ 'build', '-t', 'gcr.io/${PROJECT_ID}/${_SERVICE_NAME}:$SHORT_SHA', '.' ]
- name: 'gcr.io/cloud-builders/docker'
  args: [ 'push', 'gcr.io/${PROJECT_ID}/${_SERVICE_NAME}:$SHORT_SHA' ]
- name: 'gcr.io/cloud-builders/gcloud'
  args:
    - 'run'
    - 'deploy'
    - '${_SERVICE_NAME}'
    - '--region=${_REGION}'
    - '--platform=managed'
    - '--allow-unauthenticated'
    - '--service-account=${_SERVICE_ACCOUNT_EMAIL}'
    - '--image=gcr.io/${PROJECT_ID}/$_SERVICE_NAME}:$SHORT_SHA'
```
*However*, wihle Cloud Build says it has successfully pushed the image into Artifact Registry 
(and, in fact, keeps *finding* it when it builds new images and either whines about it or legit throws a tantrum and won't do anything further), 
the image in fact has been pushed past the event horizon of a black hole and is nowhere to be found (Cloud Logging doesn't say this but it's inferred from precise mathmatical calculations).
This is where I'd post in a chat somewhere asking if anyone has ever braved going *past* the event horizon, retrieved any lost images, 
and returned to tell the tale, but I'm going with the excuse of:

`"That's impossible, even for a computer"`

![that's impossible](./img/star-wars-impossible.jpg)


I *do* believe part of the problem may be with the quest itself, since Cloud Build logs are giving funky 404 errors during building, and *that*
seems to be what it doesn't like.

#### VI.
Skipping ahead:
`gcloud builds submit --tag gcr.io/<project_id>/quest`
just going to *pretend* the image built in the cloud alright since it built locally just fine (and gave me a nice little note saying I wasn't running it in the cloud... it was right!).
Running the image in Cloud Run in the console worked! 

[TwelveFactor](https://12factor.net/) the secret word is (nice shoto lightsaber, yoda).
![yoda green is](./img/cloud_run_console_no_lb.png)

#### VII.
Troubleshooting build
![build error](./img/cloud_logging_build_error.png)
This is the potential bug in the quest code. The build was failing for this reason, a missing `<`. Tricky. Everything is in binary in the quest repo though, and I'm not cool enough to know how to read it.
Everything *did* build locally just fine though, so it may just be that GCP has an opinionated container builder.
And my locally-built image *did* run in Cloud Run just fine, soo not everything is picky in GCP. I also just discovered that shift+enter in VIM makes a new line with an `M` *before* the current line you're writing. Neat.

#### VIII.
I'm unable to get a load balancer in GCP working because it seems like there's no way to set it to forward to port 3000. 
This is probably something I haven't come across and had I more time, I'd figure this one out.
BUT there do seem to be a lot of people online with similar feature requests, many of them for load balancing on port 22, which brings up an important point of my choice to use Cloud Run over the linux machine requested by the quest README.

The quest requests `Use Linux 64-bit x86/64 as your OS` and `Deploy the app in a Docker container`.
As much as I like SSHing into things (my laptop is Kubuntu), it entails  mutable infrastructure, which takes away a key advantage of IaC by introducing imperative configuration. An IaC model should be strictly declarative. 
One of the few times, in my opinion, anyone should ever run imperative commands in cloud infrastructure is to troubleshoot why logs or metrics aren't being collected. I'm sure there are other good times for it, but if it can be handled by pushing code, that should be preferred.

#### IX.
Let's try the Cloud Run command in the CLI:

`gcloud run deploy quest-cli --image gcr.io/<project_id>/quest --port 3000`

and it works!

#### X.
IaC: I'm out of time and I'm sorry!
If I had more time, I'd put something like this in:
```
resource "google_cloud_run_service" "default" {
  name     = "cloudrun-srv"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}
```
which is copied from [documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service) with a [port block](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#nested_ports), something like:
```
port {
  name = "cool port name"
  container_port = 3000
}
```
I would grab service account credentials:
```
gcloud iam service-accounts create quest-service-account --description="for the quest!" --display-name="quest_account"
gcloud iam service-accounts keys create quest_keys.pem --iam-account=quest-service-account@<project_id>.iam.gserviceaccount.com
```
being careful not to skip giving the account the IAM policy bindings it needs (Cloud Run, Artifact Repository, etc)
and then put the key into GitLab repository environment variables.
This would allow GitLab to run Terraform as the service account and deploy the resources!
Of course, building the image is still manual until I either have GitLab build the image or fix whatever is stopping the build in GCP.

#### XI.
Modules
Given more time, I would implement all of this into modules, and actually spent a fair amount of time modulizing the CloudFlare IaC I have into modules until I realized there's a much more efficient way to reuse the code, but then realized not every DNS record takes the same parameters, and realized I'd probably have to do a module covering `a` and `cname` records (tragically and unwisely not mentioning `aaaa` records... IPv6 is important and overlooked!), then another for `mx` records and another for `txt` records and... I'm not going to play with that tonight.
To avoid technical debt, this should absolutely be modulized, though!

#### XII.
Conclusion
I opted for fanciness and CI/CD over IaC, going straight for the bonus points and skipping the important points.
I got some fun troubleshooting time in GCP figuring out where the black hole is (and I still haven't found it), wherever those container images are but aren't. 
I was able to successfully run the image and pass every test but the load balancer which checks out since I don't have one.
Given more time, I'd get the load balancer running, since I can't believe it's a technical limitation, it's got to be a PEBKAC limitation.
I am just now realizing I spent all this time on the cool points for the DNS records and never actually implemented anything with them. 
Ideally, the IaC for the Cloud Run container would output the URL for the container (okay, okay, ideally, the Load Balancer) 
and that terraform output would be fed into a DNS record automagically.
It's how my static website module on AWS works:
```
output "website_endpoint" {
  description = "DNS endpoint of bucket"
  value       = aws_s3_bucket.root-s3-bucket.website_endpoint
}

resource "cloudflare_record" "cname-root" {
  zone_id = cloudflare_zone.zone.id
  name    = var.domain-name
  value   = module.root-s3-bucket.website_endpoint
  type    = "CNAME"
}
```
I'd really like to have `gitlab-ci.yml` run `terraform import` to import the CloudFlare zone since it's unlikely that will need to be created often.

Were I to finish, you'd visit sexbobomb.net, everything would work perfectly and it'd pass all the tests. Now I'm really sad actually, it was going to be cool. 
I tried, I cleaned up some of my old code and started a much bigger, much needed code cleanup, and I learned a lot. And I had fun! and that's what counts.
Also, my list of cool things I want to do got bigger with the CloudFlare load balancing thing, so there's that. Maybe one day I'll write a cool Medium article about it once I finish!

If there were one takeaway from this entire writeup, it's the following:

![just do it](./img/shia_just_do_it.gif)

`Don't let your dreams be dreams.`

and for context,

![it's dinnertime](./img/yzma-its-dinnertime.jpeg)


#### XIII.
BUT WAIT, THERE'S MORE!!!
I've since worked on some of the code I used in this project in other projects this past week and I wanted to add it in here for fancy points.
I also recognize I don't like that I didn't finish and aim to do so now.

terraform destroy exclude:
```
terraform state list
terraform state rm <resource>
```
or for this example:
```
terraform state list
terraform state rm cloudflare_zone.zone
terraform destroy
```
We import the zone but don't want to destroy it afterward (and likely won't be able to with an API token only, at least this one but seemingly any)

I'm running into some interesting troubleshooting. 
Start with [commit 8ffdf626](https://gitlab.com/thisisnotagroup/quest/-/commit/8ffdf626d6402d583cac846102717382f024f104).
First off, GitLab CI is failing and I'm not positive why. I'm getting a vague:
```
/bin/sh: eval: line 165: syntax error: unexpected end of file (expecting ")")
ERROR: Job failed: exit code 2
```
in the validate part of the pipeline. 

I am able to get Terraform running locally after some refining. The instance starts and everything is working as expected *except* the user data script Terraform is passing in.

These are the errors from `var/log/cloud-init-output.log` after sshing into the instance:

```
[root@ip-10-0-45-137 config]# tail /var/log/cloud-init-output.log
+ cd quest
/var/lib/cloud/instance/scripts/part-001: line 40: cd: quest: No such file or directory
+ docker build -t rearc/quest:0.1 .
/var/lib/cloud/instance/scripts/part-001: line 43: docker: command not found
+ docker run -dp 3000:3000 rearc/quest:0.1
/var/lib/cloud/instance/scripts/part-001: line 46: docker: command not found
Jul 29 05:14:46 cloud-init[3322]: util.py[WARNING]: Failed running /var/lib/cloud/instance/scripts/part-001 [127]
Jul 29 05:14:46 cloud-init[3322]: cc_scripts_user.py[WARNING]: Failed to run module scripts-user (scripts in /var/lib/cloud/instance/scripts)
Jul 29 05:14:46 cloud-init[3322]: util.py[WARNING]: Running module scripts-user (<module 'cloudinit.config.cc_scripts_user' from '/usr/lib/python2.7/site-packages/cloudinit/config/cc_scripts_user.pyc'>) failed
```
The script *is* being passed through to `/var/lib/cloud/instance/scripts/part-001` and you can even see in the logs it's running commands from the script like `cd quest` and `docker build`, 
so I'm not sure what the issue is.
Reading through `/usr/lib/python2.7/site-packages/cloudinit/config/cc_scripts_user.pyc` sure is fun, though. This is the script:
```
"""
Scripts User
------------
**Summary:** run user scripts

This module runs all user scripts. User scripts are not specified in the
``scripts`` directory in the datasource, but rather are present in the
``scripts`` dir in the instance configuration. Any cloud-config parts with a
``#!`` will be treated as a script and run. Scripts specified as cloud-config
parts will be run in the order they are specified in the configuration.
This module does not accept any config keys.

**Internal name:** ``cc_scripts_user``

**Module frequency:** per instance

**Supported distros:** all
"""

import os

from cloudinit import util

from cloudinit.settings import PER_INSTANCE

frequency = PER_INSTANCE

SCRIPT_SUBDIR = 'scripts'


def handle(name, _cfg, cloud, log, _args):
    # This is written to by the user data handlers
    # Ie, any custom shell scripts that come down
    # go here...
    runparts_path = os.path.join(cloud.get_ipath_cur(), SCRIPT_SUBDIR)
    try:
        util.runparts(runparts_path)
    except Exception:
        log.warning("Failed to run module %s (%s in %s)",
                    name, SCRIPT_SUBDIR, runparts_path)
        raise

# vi: ts=4 expandtab
```
Check out the error in the `handle` function:
```
    except Exception:
        log.warning("Failed to run module %s (%s in %s)",
                    name, SCRIPT_SUBDIR, runparts_path)
```
which turns into this:
```
cc_scripts_user.py[WARNING]: Failed to run module scripts-user (scripts in /var/lib/cloud/instance/scripts)
```
It looks like the `util.runparts` library method thingy would run any script provided by user data:
```
    try:
        util.runparts(runparts_path)
```

Off-topic, but notes for myself:
ctrl+b alt+1 or alt+2 switches orientation in tmux from horizontal to vertical

More troubleshooting:
- `which dnf` didn't work, so I changed the script to yum
- I made sure everything lined up with [Docker install on centos](https://docs.docker.com/engine/install/centos/) and...
- I'm getting 404 errors when checking ec2 sys logs, so I have no idea why Docker won't install
- I also realized I should have run `tail -n 50` instead of just `tail` earlier, oops
YUM's complaints about Docker:
```
https://download.docker.com/linux/centos/2/x86_64/stable/repodata/repomd.xml: [Errno 14] HTTPS Error 404 - Not Found
Trying other mirror.
```
I tried a few other sites that included things like `yum install docker` that produced the same 404 error before diving in.
I'm finding that this 404 error has actually been occurring for several years now from Docker! 
I wondered how much had to do with CentOS/RHEL etc and stumbled across [this beautiful piece of AWS documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-container-image.html)
and realized I'm having fun, even though it's late.
As someone who typically uses Debian, this is all unfamiliar enough.
Despite being beautiful, the suggestion to use
```
# amazon-linux-extras install docker
```
also caused a 404 error. What on earth is going on? I can't even install Docker manually! I'm on Amazon Linux 2 on 5.10 and everything. My guess is the world is burning down.

Going back to some [things I read](https://forums.docker.com/t/docker-ce-stable-x86-64-repo-not-available-https-error-404-not-found-https-download-docker-com-linux-centos-7server-x86-64-stable-repodata-repomd-xml/98965/9),
the command 
```
sed -i 's/$releasever/7/g' /etc/yum.repos.d/docker-ce.repo
```
should work, so I'm giving it a shot.

Apparently the way I'm importing the bash script into user data is deprecated and I [ought to try this](https://www.terraform.io/language/functions/templatefile) instead.

Restructuring the `.gitlab-ci.yml` variables solved the issue with the CI failing on the validate stage, but now it's saying it can't see the AWS credentials (or some other problem with them). 
At the very least, there's no exporting going on so things are less likely to show up in logs that shouldn't be there.
See commit 5a1f268e.
Not going to troubleshoot tonight.

I added a cname record in Terraform with 8cceeac but it's not showing up in CloudFlare.
Not going to troubleshoot tonight.

The `sed` command worked, but now I'm working through dependency hell and my official recommendation is to use another distro besides AZL2 for this exercise. I'd keep going, but it's late, my eyes are glazing over, and I have to be up early. Thanks for tuning in! :D

Added some load balancing! I'm out of time but it needs another subnet in another availability zone to actually `apply`.
Given more time, I'd add that with another instance, convert the instance and script to Debian, and call it a day. It's very close to working.
Given more time, I'd also edit my sleep-deprived, raw, unedited readme to be more professional, but thank you for staying with me if you made it this far :upside-down-smiley-face:

Oh also, given more time, I'd make a really nice architecture diagram. Those are essential.

